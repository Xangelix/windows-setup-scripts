$key = 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
Set-ItemProperty $key Hidden 1
Set-ItemProperty $key HideFileExt 0
Set-ItemProperty $key AutoCheckSelect 1
Set-ItemProperty $key ShowCortanaButton 0
Stop-Process -processname explorer
