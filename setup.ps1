Write-Host "Attempting Execution..."

Write-Host "Finding run location..."
$flocation = Split-Path -Parent $PSCommandPath

Write-Host "Configuring Windows Explorer..."
& $flocation\windows-explorer.ps1

Write-Host "Installing chocolatey..."
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

Write-Host "Installing chocolatey packages..."
& $flocation\chocolatey-installs.ps1
