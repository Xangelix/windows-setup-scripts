choco feature enable -n allowGlobalConfirmation
cup all
choco install 7zip
choco install adobereader
choco install barrier
choco install cheatengine
choco install discord
choco install firacode
choco install firefox-beta --pre
choco install git
choco install gitkraken
choco install googlechromebeta --pre
choco install imageglass
choco install malwarebytes
choco install mpv
choco install nordvpn
choco install notepadplusplus
choco install paint.net
choco install procexp
choco install qbittorrent
choco install steam
choco install teamviewer
choco install vscode
choco install windirstat
